# Description

Describe the bug you're reporting here.

e.g. Stories are not properly sorted when looking at recent stories in a tag

# Environment

* Operating System:
* Browser:

# Steps to Reproduce

Enumerate the steps to reproduce the bug here. Provide a link if possible.

Example:

1. Visit prismo.news
1. Click on a tag in the navigation bar.
1. Click on the 'Recent' tab.

## Expected Outcome

Describe the expected outcome of your steps here.

Example: Stories should be sorted with the most recent first.

## Actual Outcome

Describe the actual outcome of your steps here.

Example: Stories are out-of-order.
