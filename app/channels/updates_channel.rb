# frozen_string_literal: true

class UpdatesChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'updates_channel'
  end

  def unsubscribed
  end
end
