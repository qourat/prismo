# frozen_string_literal: true

class ActivityPubCommentDecorator < Draper::Decorator
  delegate_all

  def to_flag_title
    'Comment'
  end

  def excerpt
    h.strip_tags(object.content).truncate(200)
  end

  def path
    h.comment_path(object)
  end

  def to_meta_tags
    {
      title: object.root.name,
      description: excerpt,
      alternate: [{
        href: h.comment_url(object), type: 'application/activity+json'
      }],
      og: {
        image: (object.root.thumb_url(:size_200) if object.root.thumb.present?)
      }
    }
  end
end
