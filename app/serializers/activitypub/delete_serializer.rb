# frozen_string_literal: true

class ActivityPub::DeleteSerializer < ActivityPub::BaseSerializer
  def data
    {
      id: [ActivityPub::TagManager.instance.uri_for(object), '#delete'].join,
      type: 'Delete',
      actor: ActivityPub::TagManager.instance.uri_for(object.account),
      to: [ActivityPub::TagManager::COLLECTIONS[:public]],
      object: parent_object
    }
  end

  private

  def parent_object
    TombstoneSerializer.new(object).data
  end

  class TombstoneSerializer < ActivityPub::BaseSerializer
    def data
      {
        id: ActivityPub::TagManager.instance.uri_for(object),
        type: 'Tombstone'
      }
    end
  end
end
