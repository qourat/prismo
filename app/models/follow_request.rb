# frozen_string_literal: true

class FollowRequest < ApplicationRecord
  belongs_to :follower, polymorphic: true
  belongs_to :following, polymorphic: true

  validates :follower_id, uniqueness: { scope: %i[follower_type following_id following_type] }

  def authorize!
    follower.follow!(following, uri: uri)
    destroy!
  end
end
