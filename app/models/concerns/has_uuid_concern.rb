# frozen_string_literal: true

module HasUUIDConcern
  extend ActiveSupport::Concern

  included do
    after_create :reload_uuid
  end

  class_methods do
    def find(uuid)
      uuid_regexp = Rails.application.config.x.uuid_regexp
      uuid_regexp.match?(uuid.to_s.downcase) ? find_by!(uuid: uuid) : super
    end
  end

  def to_param
    uuid
  end

  private

  # See https://github.com/rails/rails/issues/17605#issuecomment-319160833
  def reload_uuid
    return true unless attributes.key?('uuid')

    self[:uuid] = self.class.where(id: id).pluck(:uuid).first
  end
end
