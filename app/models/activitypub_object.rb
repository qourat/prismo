class ActivityPubObject < ApplicationRecord
  include PgSearch
  include ActivityPub::ObjectConcern
  include HasUUIDConcern

  HOT_DAYS_LIMIT = 40

  has_closure_tree

  belongs_to :group, optional: true
  has_many :likes, as: :likeable, dependent: :destroy
  has_many :flags, as: :flaggable, dependent: :destroy
  belongs_to :root_cached, class_name: :ActivityPubObject, optional: true

  def cache_body
    update_attributes(
      content: BodyParser.new(content_source).call
    )
  end
  alias cache_content cache_body

  def cache_root
    update_attributes(
      root_cached: root
    ) if parent_id.present?
  end

  def refresh_likes_count
    update(likes_count: likes.count)
  end
end
