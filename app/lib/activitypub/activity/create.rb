# frozen_string_literal: true

class ActivityPub::Activity::Create < ActivityPub::Activity
  SUPPORTED_TYPES = %w[Note].freeze

  def perform
    return if unsupported_object_type? || invalid_origin?(@object['id'])
    return if in_reply_to.blank?

    process_comment!
  end

  private

  def process_comment!
    @comment = Comments::Create.run!(
      uri: @object['id'],
      url: object_url,
      account: @account,
      body: parsed_content,
      created_at: @object['published'],
      parent_id: in_reply_to&.id,
      auto_like: false
    )
  end

  def in_reply_to
    return nil if in_reply_to_uri.blank?

    ActivityPub::TagManager.instance.smart_uri_to_resource(in_reply_to_uri)
  end

  def in_reply_to_uri
    value_or_id(@object['inReplyTo'])
  end

  def object_url
    return if @object['url'].blank?

    url_candidate = url_to_href(@object['url'], 'text/html')

    if invalid_origin?(url_candidate)
      nil
    else
      url_candidate
    end
  end

  def parsed_content
    return '' if @object['content'].blank?

    ReverseMarkdown.convert(@object['content']).chomp('')
  end

  def unsupported_object_type?
    @object.is_a?(String) || !supported_object_type?
  end

  def supported_object_type?
    equals_or_includes_any?(@object['type'], SUPPORTED_TYPES)
  end

  def invalid_origin?(url)
    return true if unsupported_uri_scheme?(url)

    needle   = Addressable::URI.parse(url).host
    haystack = Addressable::URI.parse(@account.uri).host

    !haystack.casecmp(needle).zero?
  end
end
