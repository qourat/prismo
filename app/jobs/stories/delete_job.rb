# frozen_string_literal: true

class Stories::DeleteJob < ApplicationJob
  queue_as :default

  def perform(story_id)
    Stories::Delete.run!(story: ActivityPubPost.find(story_id))
  end
end
