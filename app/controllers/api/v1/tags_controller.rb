# frozen_string_literal: true

class Api::V1::TagsController < Api::V1Controller
  before_action :authenticate_user!

  def index
    tags = Tag.page(params[:page])
              .order(taggings_count: :desc)

    tags = tags.search(params[:q]) if params[:q].present?

    render json: tags
  end
end
