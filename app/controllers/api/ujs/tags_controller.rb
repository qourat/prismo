# frozen_string_literal: true

module Api
  module Ujs
    class TagsController < Api::Ujs::BaseController
      def toggle_follow
        user_needed
        tag = find_tag
        authorize tag

        if current_account.following?(tag)
          UnfollowService.new(current_account.object, tag).call
        else
          FollowService.new(current_account.object, tag).call
        end

        locals = {
          presenter: FollowTagButtonPresenter.new(current_account.object, tag)
        }

        render 'tags/_follow_btn', layout: false, locals: locals
      end

      private

      def find_tag
        @find_tag ||= Gutentag::Tag.find_by!(id: params[:id])
      end
    end
  end
end
