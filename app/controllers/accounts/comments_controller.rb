# frozen_string_literal: true

class Accounts::CommentsController < Accounts::BaseController
  layout 'application'

  before_action :set_account_liked_comment_ids

  def hot
    @account = find_account
    @page_title = "Hot comments by #{@account.decorate}"
    comments = CommentsQuery.new.hot
    comments = CommentsQuery.new(comments).by_account(@account)

    @comments = comments.page(params[:page])

    render :index
  end

  def recent
    @account = find_account
    @page_title = "Recent comments by #{@account.decorate}"
    comments = CommentsQuery.new.recent
    comments = CommentsQuery.new(comments).by_account(@account)

    @comments = comments.page(params[:page])

    render :index
  end
end
