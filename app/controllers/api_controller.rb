# frozen_string_literal: true

class ApiController < ActionController::Base
  include Pundit

  skip_before_action :verify_authenticity_token

  helper_method :current_account

  private

  def current_account
    current_user&.account
  end
end
