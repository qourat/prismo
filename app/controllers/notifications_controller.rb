# frozen_string_literal: true

class NotificationsController < ApplicationController
  before_action :authenticate_user!
  after_action :mark_all_as_read, only: %i[index unread]

  before_action { set_jumpbox_link(Jumpbox::NOTIFICATIONS_LINK) }

  def index
    @notifications = find_notifications.page(params[:page])
  end

  def unread
    @notifications = find_notifications.not_seen.limit(100)
    render :index
  end

  private

  def find_notifications
    @find_notifications ||= current_account.notifications_as_recipient
                                           .includes(:author, :notifable, :context)
                                           .order(id: :desc)
  end

  def mark_all_as_read
    Notifications::MarkAllAsRead.run!(account: current_account)
  end
end
