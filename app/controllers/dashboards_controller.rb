# frozen_string_literal: true

class DashboardsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_account_liked_story_ids

  before_action { set_jumpbox_link(Jumpbox::DASHBOARD_LINK) }

  def show
    @page_title = 'Hot stories'

    stories = StoriesQuery.new.hot
    stories = StoriesQuery.new(stories).for_dashboard(current_account)

    @stories = stories.page(params[:page])
  end

  def recent
    @page_title = 'Recent stories'

    stories = StoriesQuery.new.recent
    stories = StoriesQuery.new(stories).for_dashboard(current_account)

    @stories = stories.page(params[:page])

    render :show
  end
end
