# frozen_string_literal: true

class Comments::BroadcastCreation < ActiveInteraction::Base
  object :comment, class: ActivityPubComment

  def execute
    ActionCable.server.broadcast 'updates_channel', {
      event: 'comments.created',
      data: ActivityPub::CommentSerializer.new(comment)
    }
  end
end
