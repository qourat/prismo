# frozen_string_literal: true

require 'rails_helper'

describe ActivityPub::Activity::Delete do
  let(:sender) { create(:account, domain: 'example.com') }
  let(:comment) { create(:activitypub_comment, :remote, account: sender, uri: 'foobar') }

  let(:json) do
    {
      '@context': 'https://www.w3.org/ns/activitystreams',
      id: 'foo',
      type: 'Delete',
      actor: ActivityPub::TagManager.instance.uri_for(sender),
      object: ActivityPub::TagManager.instance.uri_for(comment),
      signature: 'foo'
    }.with_indifferent_access
  end

  subject { described_class.new(json, sender) }

  describe '#perform' do
    it 'soft deletes sender comment' do
      expect(Comments::Delete).to receive(:run!).with(comment: comment)
      subject.perform
    end
  end
end
