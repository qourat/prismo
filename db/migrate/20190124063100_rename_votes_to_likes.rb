class RenameVotesToLikes < ActiveRecord::Migration[5.2]
  def change
    rename_column :votes, :voteable_type, :likeable_type
    rename_column :votes, :voteable_id, :likeable_id
    rename_table :votes, :likes
  end
end
