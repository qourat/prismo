class MakeFollowRequestsPolymorphic < ActiveRecord::Migration[5.2]
  def change
    remove_foreign_key :follow_requests, :accounts
    remove_foreign_key :follows, :accounts

    rename_column :follow_requests, :account_id, :follower_id
    add_column :follow_requests, :follower_type, :string
    rename_column :follow_requests, :target_account_id, :following_id
    add_column :follow_requests, :following_type, :string

    FollowRequest.update_all(follower_type: 'Account')
    FollowRequest.update_all(following_type: 'Account')
  end
end
