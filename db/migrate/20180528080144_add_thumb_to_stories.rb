class AddThumbToStories < ActiveRecord::Migration[5.2]
  def change
    add_column :stories, :thumb_data, :jsonb, default: {}
  end
end
