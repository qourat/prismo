# frozen_string_literal: true

# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

Prismo::Application.default_url_options = Prismo::Application.config.action_mailer.default_url_options
